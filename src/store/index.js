import { createStore } from 'vuex'

import movie from './movie'

const store = createStore({
	modules: {
		movie
	}
})

export default store
