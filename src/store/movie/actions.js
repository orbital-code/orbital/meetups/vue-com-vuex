import axios from 'axios'

export function getPopularMovies(context) {
    axios.get('https://api.themoviedb.org/3/movie/popular?api_key=374e1f09ba996d9840f71bac5d4e527f')
        .then((res) => {
            context.commit('setMovies', res.data.results)
        })
}
