export function setMovies(state, value) {
    state.movies = value
}

export function addFavorite(state, value) {
    state.favorites.push(value)
}

export function deleteFavorite(state, value) {
    state.favorites = state.favorites.filter(p => p.id !== value.id)
}

export function clearAll(state) {
    state.movies = []
    state.favorites = []
}
