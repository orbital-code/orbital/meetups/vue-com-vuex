export function totalMovies(state) {
    return state.movies.length
}

export function totalFavorites(state) {
    return state.favorites.length
}

export function isFavorite(state) {
    return (id) => {
		return state.favorites.find(m => m.id === id)
	}
}